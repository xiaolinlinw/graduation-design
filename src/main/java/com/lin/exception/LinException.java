package com.lin.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Copyright(C),2023年-01月-05,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2023/1/5 9:38
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2023/1/5      版本号
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class LinException extends RuntimeException{
    private int code;
    private String linMessage;

}

package com.lin.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-28
 */
@TableName("acl_permission")
public class AclPermission implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(type= IdType.ASSIGN_UUID)
    private String id;

    private String name;

    private String permissionValue;
    @TableLogic
    private Byte isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPermissionValue() {
        return permissionValue;
    }

    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public LocalDateTime getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(LocalDateTime gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public LocalDateTime getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(LocalDateTime gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "AclPermission{" +
            "id = " + id +
            ", name = " + name +
            ", permissionValue = " + permissionValue +
            ", isDeleted = " + isDeleted +
            ", gmtCreate = " + gmtCreate +
            ", gmtModified = " + gmtModified +
        "}";
    }
}

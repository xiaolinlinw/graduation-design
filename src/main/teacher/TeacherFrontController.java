package com.lin.controller.front.teacher;

import com.lin.utils.LinResult;
import org.springframework.web.bind.annotation.*;

/**
 * Copyright(C),2022年-12月-21,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/21 16:48
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/21      版本号
 */
@RestController
@CrossOrigin
@RequestMapping("/teacher")
public class TeacherFrontController {
    @PostMapping("/login")
    public LinResult login(){
        return LinResult.success().data("token","token");
    }
    @GetMapping("/getUserInfo")
    public LinResult getUserInfo(){
        return LinResult.success().data("userId","555").data("token","lin-token-getUserInfo").data("role","admin").data("username","霖霖").data("realName","傲娇小霖霖");
    }
    @GetMapping("/logout")
    public LinResult logout(){
        return LinResult.success();
    }
}

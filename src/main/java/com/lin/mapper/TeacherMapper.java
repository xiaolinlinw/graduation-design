package com.lin.mapper;

import com.lin.entity.Teacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-01
 */
@Mapper
public interface TeacherMapper extends BaseMapper<Teacher> {

}

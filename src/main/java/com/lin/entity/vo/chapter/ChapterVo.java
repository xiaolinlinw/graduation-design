package com.lin.entity.vo.chapter;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Copyright(C),2022年-12月-24,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/24 16:24
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/24      版本号
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ChapterVo implements Serializable {
    private String title;
    private String Id;


    private Integer sort;
    private List<VideoVo> children;
}

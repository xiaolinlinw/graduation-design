package com.lin.entity.vo.front;

import lombok.Data;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/21 20:51
 * @Description 查询课程条件
 */
@Data
public class CourseConditionVo {
    private static final long serialVersionUID = 1L;
    private String title;
    private String teacherId;
    private String subjectParentId;
    private String subjectId;
    private String buyCountSort;
    private String gmtCreateSort;
    private String priceSort;

    private Byte age;
}

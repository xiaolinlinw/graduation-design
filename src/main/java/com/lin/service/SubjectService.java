package com.lin.service;

import com.lin.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.entity.vo.Classification;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-09
 */
public interface SubjectService extends IService<Subject> {
    /**
     * @param
     * @return List<Classification>
     * @date 2022/12/9 17:33
     * @description 获取分类信息链表
     */
    List<Classification> getClassification();
    /**
     * @param id
     * @return boolean
     * @date 2022/12/21 16:13
     * @description 删除课程分类信息
     */
    boolean removeSubject(String id);
}

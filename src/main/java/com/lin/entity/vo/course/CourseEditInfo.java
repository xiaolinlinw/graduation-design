package com.lin.entity.vo.course;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * Copyright(C),2022年-12月-29,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/29 14:52
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/29      版本号
 */
@Data
@NoArgsConstructor
@ToString
public class CourseEditInfo {
    private String id;
    private String title;
    private String cover;
    private Integer lessonNum;
    private String subjectParentId;
    private String subjectId;
    private String price;
    private String age;
    private String description;
}

package com.lin.mapper;

import com.lin.entity.AclUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-28
 */
@Mapper
public interface AclUserRoleMapper extends BaseMapper<AclUserRole> {

}

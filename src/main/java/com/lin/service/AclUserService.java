package com.lin.service;

import com.lin.entity.AclPermission;
import com.lin.entity.AclRole;
import com.lin.entity.AclUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-21
 */
public interface AclUserService extends IService<AclUser> {

    boolean updatePasswordByOldPassword(String id,String oldPassword, String newPassword);

    List<AclRole> getRoles(String adminId);

    List<AclPermission> getPermissions(List<String> roleIds);
}

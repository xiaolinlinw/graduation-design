package com.lin.mapper;

import com.lin.entity.AclPermission;
import com.lin.entity.AclRole;
import com.lin.entity.AclUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-21
 */
@Mapper
public interface AclUserMapper extends BaseMapper<AclUser> {

    List<AclRole> selectRoles(String adminId);

    List<AclPermission> selectPermissions(List<String> roleIds);
}

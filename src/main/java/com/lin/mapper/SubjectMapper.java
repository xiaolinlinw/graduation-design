package com.lin.mapper;

import com.lin.entity.Subject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-09
 */
@Mapper
public interface SubjectMapper extends BaseMapper<Subject> {

}

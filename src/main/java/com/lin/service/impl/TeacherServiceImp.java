package com.lin.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.aliyun.vod.upload.common.MD5Util;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.entity.AclUser;
import com.lin.entity.AclUserRole;
import com.lin.entity.Teacher;
import com.lin.mapper.TeacherMapper;
import com.lin.service.AclUserRoleService;
import com.lin.service.AclUserService;
import com.lin.service.TeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.utils.WorkNoGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-01
 */
@Service
public class TeacherServiceImp extends ServiceImpl<TeacherMapper, Teacher> implements TeacherService {
    @Autowired
    private TeacherMapper teacherMapper;
    @Autowired
    private AclUserRoleService aclUserRoleService;
    @Autowired
    private AclUserService aclUserService;

    @Override
    @Transactional
    public boolean saveTeacher(Teacher teacher) {
        StringBuilder workNo = WorkNoGenerator.getWorkNo(teacher);

        teacher.setTeacherNo(workNo.toString());
        int num = teacherMapper.insert(teacher);

        AclUserRole aclUserRole = new AclUserRole();
        aclUserRole.setUserId(teacher.getId());
        //TODO 应该使用数据查询选择
        aclUserRole.setRoleId("3");

        AclUser aclUser = new AclUser();
        aclUser.setPassword(MD5Util.md5("123"));
        aclUser.setUsername(teacher.getTeacherNo());
        aclUser.setNickName(teacher.getName());

        return aclUserRoleService.save(aclUserRole)&&num>0&&aclUserService.save(aclUser);
    }

    @Override
    public JSONObject getTeachersPage(Page<Teacher> teacherPage) {
        QueryWrapper<Teacher> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.orderByDesc("id");
        baseMapper.selectPage(teacherPage, teacherQueryWrapper);

        long current = teacherPage.getCurrent();
        long pages = teacherPage.getPages();
        boolean hasNext=true;
        boolean hasPrevious=true;
        if (current>=pages)
            hasNext=false;
        if (current<=1)
            hasPrevious=false;

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("recodes",teacherPage.getRecords());
        jsonObject.put("current",current);
        jsonObject.put("pages",pages);
        jsonObject.put("size",teacherPage.getSize());
        jsonObject.put("total",teacherPage.getTotal());
        jsonObject.put("hasNext",hasNext);
        jsonObject.put("hasPrevious",hasPrevious);
        return jsonObject;
    }

    @Override
    @Transactional
    public boolean removeTeacher(String id) {
        QueryWrapper<Teacher> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.select("teacher_no");
        teacherQueryWrapper.eq("id",id);
        Teacher teacher = baseMapper.selectOne(teacherQueryWrapper);
        String teacherNo = teacher.getTeacherNo();

        QueryWrapper<AclUser> aclUserQueryWrapper = new QueryWrapper<>();
        aclUserQueryWrapper.eq("username",teacherNo);

        return aclUserService.remove(aclUserQueryWrapper)&&baseMapper.deleteById(id)>0;
    }
}

package com.lin.entity.dvo;

import lombok.Data;
import lombok.ToString;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/30 15:16
 * @Description 统计数
 */
@Data
@ToString
public class StatisticNum {
    private Long total1;
    private Long total2;
    private Long total3;
    private Long total4;
}

package com.lin.handler;

import com.lin.exception.LinException;
import com.lin.utils.LinResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Copyright(C),2023年-01月-05,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2023/1/5 9:46
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2023/1/5      版本号
 */
@RestControllerAdvice
public class HandlerCustomException {
    @ExceptionHandler(LinException.class)
    public LinResult handleLinException(LinException e){
        return LinResult.error().code(e.getCode()).message(e.getLinMessage());
    }
}

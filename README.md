# 个人毕业设计后端
## 开发中遇到的大问题

1.shiro过滤器配置问题

```java
    Map<String, String> limit = new LinkedHashMap<>(32);
    limit.put("/preschool/course/**","anon");
    limit.put("/banner","anon");
    limit.put("/teacher/login","anon");
    limit.put("/preschool/index","anon");
    limit.put("/preschool/user/login","anon");
    limit.put("/admin/login","anon");
    limit.put("/course/uploadCover","anon");
    limit.put("/video/uploadVideo","anon");
    limit.put("/admin/subject/getSubject","anon");
    limit.put("/course/courseInfos","anon");
    limit.put("/preschool/send/**","anon");
    limit.put("/preschool/teacher/**","anon");
    // 所有请求通过我们自己的JWT Filter
    limit.put("/**","jwt");
```
shiro过滤器依次加载，前面的会覆盖后面的配置，所有必须保证有序，确保不会发送覆盖，必须使用LinkedHashMap
若依旧不能解决尝试使用
```java
 DefaultShiroFilterChainDefinition defaultShiroFilterChainDefinition = new DefaultShiroFilterChainDefinition();
        defaultShiroFilterChainDefinition.addPathDefinition("/teacher/login","anon");
        defaultShiroFilterChainDefinition.addPathDefinition("/preschool/user/login","anon");
        defaultShiroFilterChainDefinition.addPathDefinition("/admin/login","anon");
        defaultShiroFilterChainDefinition.addPathDefinition("/preschool/index","anon");
        defaultShiroFilterChainDefinition.addPathDefinition("/banner","anon");
        defaultShiroFilterChainDefinition.addPathDefinition("/course/uploadCover","anon");
        defaultShiroFilterChainDefinition.addPathDefinition("/video/uploadVideo","anon");
        defaultShiroFilterChainDefinition.addPathDefinition("/admin/subject/getSubject","anon");
        defaultShiroFilterChainDefinition.addPathDefinition("/course/courseInfos","anon");
        defaultShiroFilterChainDefinition.addPathDefinition("/preschool/send/**","anon");
        defaultShiroFilterChainDefinition.addPathDefinition("/preschool/teacher/**","anon");
        defaultShiroFilterChainDefinition.addPathDefinition("/preschool/course/**","anon");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(defaultShiroFilterChainDefinition.getFilterChainMap());
```
package com.lin.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 直播课直播地址信息
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-22
 */
@TableName("live_course")
public class LiveCourse implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程id
     */
    private String id;

    /**
     * 房间号
     */
    private String roomId;

    /**
     * 推流地址
     */
    private String pushUrl;

    /**
     * 拉流地址
     */
    private String pullUrl;

    private Byte isOverdue;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getPushUrl() {
        return pushUrl;
    }

    public void setPushUrl(String pushUrl) {
        this.pushUrl = pushUrl;
    }

    public String getPullUrl() {
        return pullUrl;
    }

    public void setPullUrl(String pullUrl) {
        this.pullUrl = pullUrl;
    }

    public Byte getIsOverdue() {
        return isOverdue;
    }

    public void setIsOverdue(Byte isOverdue) {
        this.isOverdue = isOverdue;
    }

    public LocalDateTime getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(LocalDateTime gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public LocalDateTime getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(LocalDateTime gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "LiveCourse{" +
            "id = " + id +
            ", roomId = " + roomId +
            ", pushUrl = " + pushUrl +
            ", pullUrl = " + pullUrl +
            ", isOverdue = " + isOverdue +
            ", gmtCreate = " + gmtCreate +
            ", gmtModified = " + gmtModified +
        "}";
    }
}

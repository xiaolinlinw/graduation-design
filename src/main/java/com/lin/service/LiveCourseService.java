package com.lin.service;

import com.lin.entity.LiveCourse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 直播课直播地址信息 服务类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-22
 */
public interface LiveCourseService extends IService<LiveCourse> {

    boolean createLiveURL(String roomId, String videoId);
}

package com.lin.controller;

import com.lin.utils.LinResult;
import com.lin.utils.ResultCode;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/29 10:12
 * @Description 未登录
 */
@RestController
@CrossOrigin
@RequestMapping("/Unauthenticated")
public class UnauthenticatedJumpLogin {
    @RequestMapping("/login")
    public LinResult jumpLogin(){
        return LinResult.error().code(ResultCode.UnauthenticatedException).message("跳转登录");
    }

}

package com.lin.service.impl;

import com.lin.entity.Banner;
import com.lin.mapper.BannerMapper;
import com.lin.service.BannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-21
 */
@Service
public class BannerServiceImp extends ServiceImpl<BannerMapper, Banner> implements BannerService {

}

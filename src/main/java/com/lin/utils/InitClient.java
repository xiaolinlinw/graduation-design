package com.lin.utils;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.profile.DefaultProfile;

/**
 * Copyright(C),2022年-12月-27,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/27 14:45
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/27      版本号
 */
public class InitClient {
    //填入AccessKey信息
    public static DefaultAcsClient initVodClient(String accessKeyId,String accessKeySecret){
        String regionId = "cn-shanghai";  // 点播服务接入地域
        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        return client;
    }
}

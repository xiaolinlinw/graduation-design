package com.lin.controller;

import com.lin.entity.Subject;
import com.lin.entity.vo.Classification;
import com.lin.service.SubjectService;
import com.lin.utils.LinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-09
 */
@RestController
@RequestMapping("/admin/subject")
@CrossOrigin
public class SubjectController {
    @Autowired
    private SubjectService subjectService;

    /**
     * @param
     * @return LinResult
     * @date 2022/12/9 17:32
     * @description 获取课程分类信息
     */
    @GetMapping("/getSubject")
    public LinResult getSubject() {
        List<Classification> subjectClassification = subjectService.getClassification();
        return LinResult.success().data("subjectClassification", subjectClassification);
    }

    /**
     * @param subject
     * @return LinResult
     * @date 2022/12/9 17:33
     * @description 单分类信息添加
     */
    @PostMapping("/addSubject")
    public LinResult addSubject(@RequestBody Subject subject) {
        if (subjectService.save(subject)) {
            return LinResult.success();
        }
        return LinResult.error();
    }
    /**
     * @param id
     * @return LinResult
     * @date 2023/1/5 11:54
     * @description 根据id删除分类
     */
    @DeleteMapping("/deleteSubjectById/{id}")
    public LinResult DeleteSubjectById(@PathVariable String id) {
        if (subjectService.removeSubject(id)) {
            return LinResult.success().message("删除成功");
        }
        return LinResult.error().message("删除失败");
    }
    /**
     * @param subject
     * @return LinResult
     * @date 2023/1/5 11:55
     * @description 修改分类信息
     */
    @PutMapping("/updateSubject")
    public LinResult updateSubject(@RequestBody Subject subject) {
        if (subjectService.updateById(subject)) {
            return LinResult.success().message("修改成功");
        }
        return LinResult.error().message("修改失败");
    }
}

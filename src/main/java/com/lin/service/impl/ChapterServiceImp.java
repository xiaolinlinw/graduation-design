package com.lin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lin.entity.Chapter;
import com.lin.entity.Video;
import com.lin.entity.vo.chapter.ChapterVo;
import com.lin.entity.vo.chapter.VideoVo;
import com.lin.exception.LinException;
import com.lin.mapper.ChapterMapper;
import com.lin.service.ChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.service.VideoService;
import com.lin.utils.ResultCode;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-24
 */
@Service
public class ChapterServiceImp extends ServiceImpl<ChapterMapper, Chapter> implements ChapterService {

    @Autowired
    private VideoService videoService;
    @Override
    public List<ChapterVo> getChapter(String courseId) {
        QueryWrapper<Chapter> chapterQueryWrapper = new QueryWrapper<>();
        chapterQueryWrapper.eq("course_id",courseId);
        chapterQueryWrapper.orderByAsc("sort");
        List<Chapter> chapters = baseMapper.selectList(chapterQueryWrapper);

        QueryWrapper<Video> videoQueryWrapper = new QueryWrapper<>();
        videoQueryWrapper.eq("course_id",courseId);
        chapterQueryWrapper.orderByAsc("sort");
        List<Video> videos = videoService.list(videoQueryWrapper);

        ArrayList<ChapterVo> chapterVos = new ArrayList<>();

        for (Chapter chapter : chapters) {
            ChapterVo chapterVoBean = new ChapterVo();
            BeanUtils.copyProperties(chapter,chapterVoBean);

            ArrayList<VideoVo> videoVos = new ArrayList<>();
            for(int i=videos.size()-1;i>=0;i--){
                Video video = videos.get(i);
                if(chapter.getId().equals(video.getChapterId())){
                    VideoVo videoVoBean = new VideoVo();
                    BeanUtils.copyProperties(video,videoVoBean);
                    videoVos.add(videoVoBean);
                    videos.remove(i);
                }
            }
            //按序号排序
            videoVos.sort(Comparator.comparingInt(VideoVo::getSort));
            chapterVoBean.setChildren(videoVos);
            chapterVos.add(chapterVoBean);
        }
        chapterVos.sort(Comparator.comparingInt(ChapterVo::getSort));
        return chapterVos;
    }

    @Override
    @Transactional
    public boolean removeChapter(String chapterId) {
        QueryWrapper<Video> videoQueryWrapper = new QueryWrapper<>();
        videoQueryWrapper.eq("chapter_Id",chapterId);
        long count = videoService.count(videoQueryWrapper);
        int num = 0;
        if (count>0){
            if (videoService.deleteVideoBatch(chapterId)) {
                num = baseMapper.deleteById(chapterId);
            }
        }else {
            num=baseMapper.deleteById(chapterId);
        }
        return num>0;
    }

    @Override
    public boolean saveChapter(Chapter chapter) {
        QueryWrapper<Chapter> chapterQueryWrapper = new QueryWrapper<>();
        chapterQueryWrapper.eq("course_id",chapter.getCourseId());
        chapterQueryWrapper.eq("sort",chapter.getSort());
        Long count = baseMapper.selectCount(chapterQueryWrapper);
        if (count>=1){
            throw new LinException(ResultCode.ERROR,"第"+chapter.getSort()+"章节已存在,请修改后添加");
        }
        return baseMapper.insert(chapter)>0;
    }

    @Override
    public boolean updateByChapter(Chapter chapter) {
        QueryWrapper<Chapter> chapterQueryWrapper = new QueryWrapper<>();
        chapterQueryWrapper.eq("course_id",chapter.getCourseId());
        chapterQueryWrapper.eq("sort",chapter.getSort());
        chapterQueryWrapper.ne("id",chapter.getId());
        Long count = baseMapper.selectCount(chapterQueryWrapper);
        if (count>0){
            throw new LinException(ResultCode.SORT_EXISTS,"第"+chapter.getSort()+"章节已存在是否互换");
        }

        return baseMapper.updateById(chapter)>0;
    }

    @Override
    public boolean swapUpdateChapter(Chapter chapter) {
        QueryWrapper<Chapter> oldChapterQueryWrapper = new QueryWrapper<>();
        oldChapterQueryWrapper.eq("id",chapter.getId());
        oldChapterQueryWrapper.select("sort");
        Chapter oldChapter = baseMapper.selectOne(oldChapterQueryWrapper);
        QueryWrapper<Chapter> chapterQueryWrapper = new QueryWrapper<>();
        chapterQueryWrapper.eq("course_id",chapter.getCourseId());
        chapterQueryWrapper.eq("sort",chapter.getSort());
        chapterQueryWrapper.ne("id",chapter.getId());
        chapterQueryWrapper.select("id");
        Chapter conflictChapter = baseMapper.selectOne(chapterQueryWrapper);
        conflictChapter.setSort(oldChapter.getSort());
        return baseMapper.updateById(conflictChapter)>0&&baseMapper.updateById(chapter)>0;
    }

    @Override
    @Transactional
    public boolean removeLiveChapter(String chapterId) {
        QueryWrapper<Video> videoQueryWrapper = new QueryWrapper<>();
        videoQueryWrapper.eq("chapter_id",chapterId);
//        long count = videoService.count(videoQueryWrapper);
//        if (count>0)
        videoService.remove(videoQueryWrapper);
        return baseMapper.deleteById(chapterId)>0;
    }

}

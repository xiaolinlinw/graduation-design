package com.lin.entity.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Copyright(C),2022年-12月-09,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/9 16:38
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/9      版本号
 */
@Data
@ToString
@NoArgsConstructor
public class ClassificationTwo {
    private String title;
    private String key;
    private int level=2;
}

package com.lin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lin.entity.StatisticsDaily;
import com.lin.entity.UcenterMember;
import com.lin.entity.dvo.StatisticMonthDVO;
import com.lin.entity.dvo.StatisticNum;
import com.lin.entity.vo.StatisticMonthVo;
import com.lin.entity.vo.StatisticVo;
import com.lin.mapper.StatisticsDailyMapper;
import com.lin.service.CourseService;
import com.lin.service.StatisticsDailyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.service.UcenterMemberService;
import com.lin.service.VideoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-30
 */
@Service
public class StatisticsDailyServiceImp extends ServiceImpl<StatisticsDailyMapper, StatisticsDaily> implements StatisticsDailyService {

    @Autowired
    @Lazy
    private UcenterMemberService userService;
    @Autowired
    private VideoService videoService;
    @Autowired
    private CourseService courseService;

    @Override
    public List<StatisticVo> getStatisticInfo() {

        StatisticNum totals = baseMapper.selectStatisticTotal();
        StatisticNum months = baseMapper.selectStatisticMonth();

        return Arrays.asList(
                new StatisticVo("注册数", "visit-count|svg", months.getTotal1(), totals.getTotal1(), "green", "月"),
                new StatisticVo("登录数", "total-sales|svg", months.getTotal2(), totals.getTotal2(), "blue", "月"),
                new StatisticVo("观看数", "transaction|svg", months.getTotal3(), totals.getTotal3(), "purple", "月"),
                new StatisticVo("课程数", "download-count|svg", months.getTotal4(), totals.getTotal4(), "orange", "月")
        );
    }

    @Override
    public List<StatisticsDaily> getStatisticTrendInfo() {

        return baseMapper.getTrend();
    }

    @Override
    public List<StatisticMonthVo> getStatisticMonthInfo() {
        List<StatisticMonthDVO> visitNum = baseMapper.getVisitNum();


        return visitNum.stream().map(x -> new StatisticMonthVo((x.getStatisticYear() + "年" + x.getStatisticMonth() + "月"), x.getVideoVideoNum())).collect(Collectors.toList());
    }

    @Override
    public void saveStatistic(LocalDateTime yesterdays) {
        Long registerNum = userService.statisticsCount(yesterdays);
        Long viewVideoNum = videoService.statisticsCount(yesterdays);
        Long courseNum = courseService.statisticsCount(yesterdays);
        StatisticsDaily statisticsDaily = new StatisticsDaily();
        statisticsDaily.setCourseNum(courseNum.intValue());
        statisticsDaily.setRegisterNum(registerNum.intValue());
        statisticsDaily.setDateCalculated(String.valueOf(LocalDate.now()));
        statisticsDaily.setVideoViewNum(viewVideoNum.intValue());

        QueryWrapper<StatisticsDaily> statisticsDailyQueryWrapper = new QueryWrapper<>();
        statisticsDailyQueryWrapper.eq("date_calculated", LocalDate.now().plusDays(-1));
        StatisticsDaily statisticsDailyDB = baseMapper.selectOne(statisticsDailyQueryWrapper);
        if (statisticsDailyDB==null){
            baseMapper.insert(statisticsDaily);
        }else {
            BeanUtils.copyProperties(statisticsDaily,statisticsDailyDB);
            baseMapper.updateById(statisticsDailyDB);
        }

    }

    @Override
    public boolean setStatisticCount() {
        QueryWrapper<StatisticsDaily> statisticsDailyQueryWrapper = new QueryWrapper<>();
        statisticsDailyQueryWrapper.eq("date_calculated", LocalDate.now());
        StatisticsDaily statisticsDaily = baseMapper.selectOne(statisticsDailyQueryWrapper);
        if (statisticsDaily == null) {
            statisticsDaily = new StatisticsDaily();
            statisticsDaily.setLoginNum(1);
           return baseMapper.insert(statisticsDaily)>0;
        } else {
            statisticsDaily.setLoginNum(statisticsDaily.getLoginNum()+1);
            return baseMapper.updateById(statisticsDaily)>0;
        }
    }
}

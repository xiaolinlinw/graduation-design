package com.lin.service.impl;

import com.lin.entity.AclUserRole;
import com.lin.mapper.AclUserRoleMapper;
import com.lin.service.AclUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-28
 */
@Service
public class AclUserRoleServiceImp extends ServiceImpl<AclUserRoleMapper, AclUserRole> implements AclUserRoleService {

}

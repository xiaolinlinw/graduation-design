package com.lin.service;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.entity.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-01
 */
public interface TeacherService extends IService<Teacher> {
    /**
     * @param teacher
     * @return boolean
     * @date 2022/12/5 10:39
     * @description 添加教师信息，返回是否添加成功
     */
    boolean saveTeacher(Teacher teacher);

    JSONObject getTeachersPage(Page<Teacher> teacherPage);

    boolean removeTeacher(String id);
}

package com.lin.handler;

import com.lin.utils.LinResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * Copyright(C),2022年-12月-03,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/3 11:19
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/3      版本号
 */
@RestControllerAdvice
public class HandlerSQLException {
    // SQL完整性约束冲突异常
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public LinResult handlerSQLIntegrityConstraintViolationException(SQLIntegrityConstraintViolationException e){
        if (e.getMessage().startsWith("Duplicate entry")){
            String[] errorInfo = e.getMessage().split(" ");
            return LinResult.error().message(errorInfo[2]+"数据已存在");
        }
        return LinResult.error().message(e.getMessage());
    }
}

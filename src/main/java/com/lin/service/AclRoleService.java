package com.lin.service;

import com.lin.entity.AclRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-28
 */
public interface AclRoleService extends IService<AclRole> {

}

package com.lin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class PreschoolEducationApplication {

    public static void main(String[] args) {
        SpringApplication.run(PreschoolEducationApplication.class, args);
    }

}

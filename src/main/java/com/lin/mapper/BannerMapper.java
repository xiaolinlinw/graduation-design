package com.lin.mapper;

import com.lin.entity.Banner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-21
 */
@Mapper
public interface BannerMapper extends BaseMapper<Banner> {

}

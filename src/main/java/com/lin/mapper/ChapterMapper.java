package com.lin.mapper;

import com.lin.entity.Chapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-24
 */
@Mapper
public interface ChapterMapper extends BaseMapper<Chapter> {

}

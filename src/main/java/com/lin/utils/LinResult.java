package com.lin.utils;

import com.alibaba.fastjson2.JSONObject;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;


/**
 * Copyright(C),2022年-11月-30,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/11/30 19:17
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/11/30      版本号
 */
@Data
@ToString
public class LinResult implements Serializable {
    private boolean type;
    private int code;
    private String message;
    private JSONObject result=new JSONObject();

    private LinResult(){}

    public static  LinResult success(){
        LinResult linResult = new LinResult();
        linResult.setType(true);
        linResult.setCode(ResultCode.SUCCESS);
        linResult.setMessage("操作成功");
        return linResult;
    }
    public static  LinResult error(){
        LinResult linResult = new LinResult();
        linResult.setType(false);
        linResult.setMessage("操作失败");
        linResult.setCode(ResultCode.ERROR);
        return linResult;
    }


    public LinResult data(String key,Object value){
        this.result.put(key,value);
        return this;
    }
    public LinResult data(JSONObject result){
        this.setResult(result);
        return this;
    }

    public LinResult message(String message){
        this.setMessage(message);
        return this;
    }
    public LinResult code(int code){
        this.setCode(code);
        return this;
    }

}

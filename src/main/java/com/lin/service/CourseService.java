package com.lin.service;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.entity.Course;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.entity.Video;
import com.lin.entity.vo.course.CourseInfoVo;
import com.lin.entity.vo.front.CourseConditionVo;
import com.lin.entity.vo.front.CourseFrontInfoVo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-23
 */
public interface CourseService extends IService<Course> {
    /**
     * @param file
     * @return String
     * @date 2023/1/6 9:38
     * @description 实现课程封面上传
     */
    String saveCover(MultipartFile file) throws IOException;
    /**
     * @param courseId
     * @return CourseInfoVo
     * @date 2023/1/6 9:39
     * @description 根据id获取课程信息
     */
    CourseInfoVo getCourseInfo(String courseId);
    /**
     * @param page
     * @param title
     * @param startTime
     * @param endTime
     * @param order
     * @param type
     * @return IPage<CourseInfoVo>
     * @date 2023/1/6 9:40
     * @description 获取课程信息用于展示
     */
    IPage<CourseInfoVo> getCourseInfoVos(IPage<CourseInfoVo> page, String title, String startTime, String endTime, String order, Byte type,String teacherId);
    /**
     * @param courseId
     * @return boolean
     * @date 2023/1/6 9:43
     * @description 删除课程及其章节课时
     */
    boolean removeCourse(String courseId);

    Map<String,Object> getLiveCourseInfoVos(IPage<CourseInfoVo> page, String title, String startTime, String endTime, String order, Byte type, String teacherId);
    /**
     * @param video
     * @return boolean
     * @date 2023/1/5 12:06
     * @description 添加直播课时信息，添加时判断排序信息是否存在，开课时间定位第一课时的第一节
     */
    boolean saveLiveVideo(Video video);
    /**
     * @param video
     * @return boolean
     * @date 2023/1/6 11:57
     * @description 修改课时信息并保证课时顺序不会重复
     */
    boolean updateLiveVideo(Video video);
    /**
     * @param video
     * @return boolean
     * @date 2023/1/7 17:56
     * @description 小节互换修改信息
     */
    boolean swapUpdateLiveVideo(Video video);

    boolean updateLiveVideoStartTime(Video video);

    boolean removeLiveCourse(String courseId);

    CourseFrontInfoVo getFrontCourseInfo(String courseId);

    JSONObject getCourseInfoByCondition(Page<Course> coursePage, CourseConditionVo courseConditionVo, Byte type);

    Long statisticsCount(LocalDateTime yesterdays);
}

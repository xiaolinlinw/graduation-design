package com.lin.utils;

import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.lin.entity.Teacher;
import org.joda.time.DateTime;

/**
 * Copyright(C),2022年-12月-05,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/5 9:41
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/5      版本号
 */
public class WorkNoGenerator {
    public static StringBuilder getWorkNo(Teacher teacher){
        // 生称固定格式工号
        StringBuilder teacherNo=new StringBuilder("LIN");
        String year = new DateTime().toString("yyyy");
        //使用mp带的雪花算法
        String suffix = new DefaultIdentifierGenerator().nextId(teacher).toString();
        return teacherNo.append(year).append(suffix.substring(suffix.length()-6));
    }
}

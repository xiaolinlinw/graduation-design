package com.lin.service;

import com.lin.entity.Chapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.entity.vo.chapter.ChapterVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-24
 */
public interface ChapterService extends IService<Chapter> {
    /**
     * @param courseId
     * @return List<ChapterVo>
     * @date 2023/1/6 9:38
     * @description 获取课时结构信息
     */
    List<ChapterVo> getChapter(String courseId);
    /**
     * @param chapterId
     * @return boolean
     * @date 2023/1/5 12:02
     * @description 根据id删除章节信息，同时删除该章节下的课时信息
     */
    boolean removeChapter(String chapterId);

    boolean saveChapter(Chapter chapter);
    /**
     * @param chapter
     * @return boolean
     * @date 2023/1/7 10:52
     * @description 修改章节信息
     */
    boolean updateByChapter(Chapter chapter);
    /**
     * @param chapter
     * @return boolean
     * @date 2023/1/7 10:59
     * @description 确认交互顺序修改
     */
    boolean swapUpdateChapter(Chapter chapter);

    boolean removeLiveChapter(String chapterId);
}

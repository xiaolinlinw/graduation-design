package com.lin.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lin.entity.vo.course.CourseInfoVo;
import com.lin.entity.vo.front.CourseFrontInfoVo;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDateTime;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 *
 * @author 霖霖
 * @since 2022-12-23
 */
@Mapper
public interface CourseMapper extends BaseMapper<Course> {

    CourseInfoVo selectCourseInfoById(String courseId);

    IPage<CourseInfoVo> selectCourseInfoVo(IPage<CourseInfoVo> page, String teacherId, String title, String startTime, String endTime, String order, Byte type);

    IPage<CourseInfoVo> selectLiveCourseInfoVo(Page<CourseInfoVo> page, String teacherId, String title, String startTime, String endTime, String order, Byte type);

    CourseFrontInfoVo selectFrontCourseInfoVo(String courseId);

    Long selectStatisticCount(LocalDateTime yesterdays);
}

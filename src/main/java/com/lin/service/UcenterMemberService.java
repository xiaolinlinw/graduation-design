package com.lin.service;

import com.lin.entity.AclPermission;
import com.lin.entity.AclRole;
import com.lin.entity.UcenterMember;
import com.baomidou.mybatisplus.extension.service.IService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-23
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    boolean sendSms(String phone, Map<String, Object> map) throws ExecutionException, InterruptedException;

    List<AclRole> getRoles(String userId);

    List<AclPermission> getPermissions(List<String> roleIds);

    boolean registerUser(UcenterMember user);

    Long statisticsCount(LocalDateTime yesterday);
}

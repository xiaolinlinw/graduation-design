package com.lin.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.aop.WebLog;
import com.lin.entity.Teacher;
import com.lin.service.TeacherService;
import com.lin.utils.LinResult;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-01
 */
@RestController
@RequestMapping("/admin/teacher")
@CrossOrigin
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    /**
     * @param currentPage
     * @param pageSize
     * @param name
     * @param level
     * @param startTime
     * @param endTime
     * @param order
     * @return LinResult
     * @date 2022/12/5 9:29
     * @description 分页查询教师信息，教师列表展示
     */
//    @RequiresRoles("admin")
    @WebLog("获取教师列表信息")
    @GetMapping("/getTeacherList")
    public LinResult getTeacherListTwo(@RequestParam(value = "currentPage", defaultValue = "1") long currentPage,
                                       @RequestParam(value = "pageSize", defaultValue = "10") long pageSize,
                                       @RequestParam(value = "name", required = false) String name,
                                       @RequestParam(value = "level", required = false) Integer level,
                                       @RequestParam(value = "startTime", required = false) String startTime,
                                       @RequestParam(value = "endTime", required = false) String endTime,
                                       @RequestParam(value = "order", required = false) String order) {
        LambdaQueryWrapper<Teacher> teacherLambdaQueryWrapper = new LambdaQueryWrapper<>();
        teacherLambdaQueryWrapper.like(StringUtils.isNotEmpty(name), Teacher::getName, name);
        teacherLambdaQueryWrapper.between(StringUtils.isNotEmpty(startTime), Teacher::getGmtCreate, startTime, endTime);
        teacherLambdaQueryWrapper.eq(level != null, Teacher::getLevel, level);
        teacherLambdaQueryWrapper.orderBy(StringUtils.isNotEmpty(order), "ascend".equals(order), Teacher::getGmtCreate);
        Page<Teacher> teacherPage = new Page<>(currentPage, pageSize);
        teacherService.page(teacherPage, teacherLambdaQueryWrapper);
        return LinResult.success().data("list", teacherPage.getRecords()).data("total", teacherPage.getTotal());
    }

    /**
     * @param id
     * @return LinResult
     * @date 2022/12/5 9:29
     * @description 根据id删除教师信息，使用逻辑删除
     */
    @GetMapping("/deleteTeacherById/{id}")
    public LinResult deleteById(@PathVariable("id") String id) {
        if (teacherService.removeTeacher(id)) {
            return LinResult.success();
        } else {
            return LinResult.error();
        }

    }

    /**
     * @param ids
     * @return LinResult
     * @date 2023/1/5 11:55
     * @description 批量删除教师信息
     */
    @PostMapping("/deleteTeacherByIds")
    public LinResult deleteByIdBatch(@RequestBody String[] ids) {
        if (teacherService.removeBatchByIds(Arrays.asList(ids))) {
            return LinResult.success().message("批量删除成功");
        }
        return LinResult.error().message("批量删除失败");
    }

    /**
     * @param teacher
     * @return LinResult
     * @date 2022/12/5 9:29
     * @description 添加教师
     */
    @PostMapping("/addTeacher")
    public LinResult addTeacher(@Validated @RequestBody Teacher teacher) {
        if (teacherService.saveTeacher(teacher)) {
            return LinResult.success().message("添加成功").data("teacherNo", teacher.getTeacherNo());
        } else {
            return LinResult.error().message("添加失败!请重试");
        }
    }

    /**
     * @param name
     * @return LinResult
     * @date 2022/12/5 9:29
     * @description 添加教师
     */
    @WebLog("异步判断")
    @GetMapping("/isNameExists")
    public LinResult isNameExists(@RequestParam("name") String name) {
        QueryWrapper<Teacher> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.eq("name", name);
        if (teacherService.count(teacherQueryWrapper) > 0) {
            return LinResult.error().message("教师名" + name + "已存在");
        } else {
            return LinResult.success().message("通过");
        }
    }

    /**
     * @param id
     * @return LinResult
     * @date 2022/12/5 9:21
     * @description 根据Id查询教师信息
     */
    @GetMapping("/getTeacherInfoBYId/{id}")
    public LinResult getTeacherInfoBYId(@PathVariable("id") String id) {
        Teacher teacher = teacherService.getById(id);
        if (teacher != null) {
            return LinResult.success().data("teacher", teacher);
        }
        return LinResult.error().message("教师不存在");
    }

    /**
     * @param teacher
     * @return LinResult
     * @date 2022/12/5 10:37
     * @description 更能Id修改教师信息
     */
    @PutMapping("/updateTeacher")
    public LinResult updateTeacher(@Validated @RequestBody Teacher teacher) {
        if (teacherService.updateById(teacher)) {
            return LinResult.success().message("修改成功");
        }
        return LinResult.error().message("修改失败");
    }
}

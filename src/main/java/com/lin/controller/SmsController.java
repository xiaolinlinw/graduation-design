package com.lin.controller;

import com.aliyun.vod.upload.common.MD5Util;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lin.entity.UcenterMember;
import com.lin.exception.LinException;
import com.lin.service.StatisticsDailyService;
import com.lin.service.UcenterMemberService;
import com.lin.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/23 22:43
 * @Description 短信服务
 */
@RestController
@CrossOrigin
@RequestMapping("preschool")
public class SmsController {
    @Autowired
    private UcenterMemberService userService;
    @Autowired
    private StatisticsDailyService statisticsDailyService;

    @GetMapping("/send/{phone}")
    public LinResult sendSms(@PathVariable("phone") String phone,
                             HttpSession session) throws ExecutionException, InterruptedException {
//        UcenterMember isExistsPhone = userService.getOne(new QueryWrapper<UcenterMember>().eq("mobile", phone));
//        if (isExistsPhone!=null){
//            return LinResult.error().message("手机号已存在");
//        }
        String sixBitRandom = RandomUtil.getSixBitRandom();
        Map<String, Object> map = new HashMap<>();
        map.put("code", sixBitRandom);

        if (userService.sendSms(phone, map)) {
            session.setAttribute(phone, sixBitRandom);
            MyTimer.timerTask(session, phone, 60);
            return LinResult.success().message("短信验证码发送成功");
        }
        System.out.println(session.getId());
        return LinResult.error().message("短信发送失败");
    }

    @PostMapping("user")
    public LinResult register(@RequestBody UcenterMember user, HttpSession session) {
        System.out.println(session.getAttribute(user.getMobile()));
        String code = (String) session.getAttribute(user.getMobile());
        if (code == null || code.isEmpty()) {
            return LinResult.error().message("验证码不存在或已过期");
        } else if (code.equals(user.getValidCode())) {
            user.setPassword(MD5Util.md5(user.getPassword()));
            return userService.registerUser(user) ? LinResult.success() : LinResult.error();
        } else {
            return LinResult.error().message("验证码错误");
        }
    }

    @PostMapping("user/login")
    public LinResult login(@RequestBody UcenterMember user){
        user.setPassword(MD5Util.md5(user.getPassword()));
        QueryWrapper<UcenterMember> ucenterMemberQueryWrapper = new QueryWrapper<>();
        ucenterMemberQueryWrapper.eq("mobile",user.getMobile());
        ucenterMemberQueryWrapper.eq("password",user.getPassword());
        UcenterMember one = userService.getOne(ucenterMemberQueryWrapper);
        if (one!=null) {
            if (one.getIsDisabled())
                throw new LinException(ResultCode.USER_IS_DISABLE,"账号已被禁用，请联系管理员");
            statisticsDailyService.setStatisticCount();
            String jwtToken = JWTUtil.getJwtToken(one.getId(), one.getNickname());
            return LinResult.success().data("token",jwtToken);
        }else {
            return LinResult.error().message("账号或密码错误");
        }
    }
}

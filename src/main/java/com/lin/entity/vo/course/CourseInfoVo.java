package com.lin.entity.vo.course;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * Copyright(C),2022年-12月-27,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/27 17:39
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/27      版本号
 */
@Data
@ToString
@NoArgsConstructor
public class CourseInfoVo {
    private String id;
    private String title;
    private String cover;
    private Integer lessonNum;
    private String subjectLevelOne;
    private String subjectLevelTwo;
    private String teacher;
    private String price;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime startTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime gmtCreate;
}

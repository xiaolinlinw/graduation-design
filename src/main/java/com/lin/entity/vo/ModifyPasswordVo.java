package com.lin.entity.vo;

import lombok.Data;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/21 14:45
 * @Description 修改密码VO
 */
@Data
public class ModifyPasswordVo {
    String oldPassword;
    String newPassword;
}

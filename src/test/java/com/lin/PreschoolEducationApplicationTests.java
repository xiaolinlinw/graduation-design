package com.lin;

import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.lin.entity.Teacher;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;

@SpringBootTest
class PreschoolEducationApplicationTests {

    @Test
    void contextLoads() {
    }
    @Test
    void codeGeneratorMybatisPlus(){
        FastAutoGenerator.create("jdbc:mysql://192.168.1.20:3306/preschool?useSSL=false&serverTimezone=GMT%2B8&characterEncoding=utf-8", "root", "024680")
                .globalConfig(builder -> {
                    builder.author("霖霖")
                            .disableOpenDir()// 设置作者
//                            .enableSwagger() // 开启 swagger 模式
                            .outputDir("D:\\Code\\Java\\preschool\\src\\main\\java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.lin") // 设置父包名
//                            .moduleName("system") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, "D:\\Code\\Java\\preschool\\src\\main\\resources\\mappers")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder
                            .addInclude("statistics_daily");// 设置需要生成的表名
//                            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
                })
                .strategyConfig(builder -> {
                    builder.controllerBuilder()
                            //开启生成@RestController 控制器
                            .enableRestStyle();
                })
                .strategyConfig(builder -> {
                    builder.serviceBuilder()
                            .formatServiceFileName("%sService")
                            .formatServiceImplFileName("%sServiceImp");
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
    @Test
    void dateTest(){
        String year = new DateTime().toString("yyyy");
        System.out.println(year);
    }
    @Test
    void noTest(){
        DefaultIdentifierGenerator defaultIdentifierGenerator = new DefaultIdentifierGenerator();
        System.out.println("defaultIdentifierGenerator.nextId() = " + defaultIdentifierGenerator.nextId(new Teacher()));//1599242066469117953
    }

}

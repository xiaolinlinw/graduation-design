package com.lin.entity.vo.chapter;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * Copyright(C),2022年-12月-24,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/24 16:28
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/24      版本号
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VideoVo {
    private String id;
    private String title;
    private Integer sort;
    private String videoSourceId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime startTime;
}

package com.lin.controller.front.teacher;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lin.entity.Subject;
import com.lin.entity.vo.ClassificationTwo;
import com.lin.service.SubjectService;
import com.lin.utils.LinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright(C),2022年-12月-23,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/23 8:54
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/23      版本号
 */
@RestController
@CrossOrigin

@RequestMapping("/subject")
public class SubjectFrontController {
    @Autowired
    private SubjectService subjectService;
    @GetMapping("/getSubjectTwo/{parentId}")
    public LinResult getSubjectTwo(@PathVariable("parentId") String subjectParentId){
        QueryWrapper<Subject> subjectQueryWrapper = new QueryWrapper<>();
        subjectQueryWrapper.eq("parent_id",subjectParentId);
        List<Subject> subjectList = subjectService.list(subjectQueryWrapper);

        ArrayList<ClassificationTwo> result = new ArrayList<>();
        for (Subject subject : subjectList) {
            ClassificationTwo bean = new ClassificationTwo();
            bean.setKey(subject.getId());
            bean.setTitle(subject.getTitle());
            result.add(bean);
        }

        return LinResult.success().data("subjectTwo",result);
    }
}

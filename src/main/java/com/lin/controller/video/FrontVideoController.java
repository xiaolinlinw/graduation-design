package com.lin.controller.video;

import com.aliyuncs.exceptions.ClientException;
import com.lin.aop.WebLog;
import com.lin.service.VideoService;
import com.lin.utils.LinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/21 19:57
 * @Description 用户视频
 */
@RestController
@CrossOrigin
@RequestMapping("video")
public class FrontVideoController {
    @Autowired
    private VideoService videoService;
    @WebLog("获取视频授权")
    @GetMapping("getPlayAuth/{videoSourceId}")
    public LinResult getAuthPlay(@PathVariable("videoSourceId") String videoSourceId) throws ClientException {
        String playAuth = videoService.getPlayAuth(videoSourceId);
        videoService.updatePlayCount(videoSourceId);
        return LinResult.success().data("playAuth",playAuth);
    }
}

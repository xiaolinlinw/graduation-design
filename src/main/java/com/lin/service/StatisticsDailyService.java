package com.lin.service;

import com.lin.entity.StatisticsDaily;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.entity.vo.StatisticMonthVo;
import com.lin.entity.vo.StatisticVo;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-30
 */
public interface StatisticsDailyService extends IService<StatisticsDaily> {

    List<StatisticVo> getStatisticInfo();

    List<StatisticsDaily> getStatisticTrendInfo();

    List<StatisticMonthVo> getStatisticMonthInfo();

    void saveStatistic(LocalDateTime yesterdays);

    boolean setStatisticCount();
}

package com.lin.controller.user;

import com.lin.service.BannerService;
import com.lin.utils.LinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/21 17:08
 * @Description 轮播图展示
 */
@RestController
@CrossOrigin
@RequestMapping("banner")
public class BannerController {
    @Autowired
    private BannerService bannerService;

    @GetMapping
    public LinResult getBanner() {
        return LinResult.success().data("list",bannerService.list());
    }
}

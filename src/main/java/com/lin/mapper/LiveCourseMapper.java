package com.lin.mapper;

import com.lin.entity.LiveCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 直播课直播地址信息 Mapper 接口
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-22
 */
@Mapper
public interface LiveCourseMapper extends BaseMapper<LiveCourse> {

}

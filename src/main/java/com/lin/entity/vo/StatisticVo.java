package com.lin.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/30 14:01
 * @Description 统计视图
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StatisticVo {
    private String title;
    private String icon;
    private Long value;
    private Long total;
    private String color;
    private String action;
}

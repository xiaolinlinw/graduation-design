package com.lin.scheduled;

import com.lin.service.StatisticsDailyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/30 19:13
 * @Description 统计任务
 */
@Component
public class ScheduledTask {
    @Autowired
    private StatisticsDailyService statisticsDailyService;

    @Scheduled(cron = "0 0 1 * * ?")
    public void task(){
        statisticsDailyService.saveStatistic(LocalDateTime.now().plusDays(-1));
    }
}

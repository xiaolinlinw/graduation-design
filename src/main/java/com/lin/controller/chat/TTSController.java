package com.lin.controller.chat;

import com.lin.utils.TTSAuth;
import com.lin.utils.LinResult;
import com.lin.utils.TTSUtil;
import org.springframework.web.bind.annotation.*;

/**
 * Copyright(C),2022年-2022,霖霖
 *
 * @author 霖霖
 * @version 1.0
 * @date 2023/5/1 16:57
 * @Description
 */
@RestController
@CrossOrigin
@RequestMapping("chat")
public class TTSController {
    @GetMapping("tts")
    public LinResult getTTsURL(@RequestParam("text") String text) throws Exception {
        return LinResult.success().data("url", TTSAuth.init(text));
    }
    @GetMapping("tts2")
    public LinResult getTTs(@RequestParam("text") String text) {
        return LinResult.success().data("tts", TTSUtil.init(text));
    }
}

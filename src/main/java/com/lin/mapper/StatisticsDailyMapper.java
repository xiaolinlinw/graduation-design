package com.lin.mapper;

import com.lin.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lin.entity.dvo.StatisticMonthDVO;
import com.lin.entity.dvo.StatisticNum;
import com.lin.entity.vo.StatisticVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-30
 */
@Mapper
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

    StatisticNum selectStatisticTotal();

    StatisticNum selectStatisticMonth();

    List<StatisticsDaily> getTrend();

    List<StatisticMonthDVO> getVisitNum();
}

package com.lin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.entity.AclUser;
import com.lin.entity.Teacher;
import com.lin.entity.Video;
import com.lin.entity.vo.course.CourseInfoVo;
import com.lin.service.AclUserService;
import com.lin.service.CourseService;
import com.lin.service.TeacherService;
import com.lin.utils.JWTUtil;
import com.lin.utils.LinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Copyright(C),2023年-01月-03,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2023/1/3 19:15
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2023/1/3      版本号
 */
@CrossOrigin
@RestController
@RequestMapping("/course")
public class LiveCourseController {
    @Autowired
    private CourseService courseService;
    @Autowired
    private AclUserService userService;

    @Autowired
    private TeacherService teacherService;
    /**
     * @param currentPage
     * @param pageSize
     * @param type
     * @param title
     * @param startTime
     * @param endTime
     * @param order
     * @return LinResult
     * @date 2023/1/5 11:53
     * @description 条件分页获取直播课程信息
     */
    @GetMapping("/liveCourse")
    public LinResult getLiveCourse(@RequestParam(value = "currentPage",required = false,defaultValue = "1") long currentPage,
                                   @RequestParam(value = "pageSize",required = false,defaultValue = "10") long pageSize,
                                   @RequestParam(value = "type",required = false,defaultValue = "1") Byte type,
                                   @RequestParam(value = "title",required = false) String title,
                                   @RequestParam(value = "startTime",required = false) String startTime,
                                   @RequestParam(value = "endTime",required = false) String endTime,
                                   @RequestParam(value = "order",required = false) String order,
                                   HttpServletRequest request){

        Page<CourseInfoVo> coursePage = new Page<>(currentPage, pageSize);
        String teacherId = JWTUtil.getMemberIdByJwtToken(request);
        AclUser teacherACl = userService.getById(teacherId);
        QueryWrapper<Teacher> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.eq("teacher_no",teacherACl.getUsername());
        teacherQueryWrapper.select("id");
        Teacher teacher  = teacherService.getOne(teacherQueryWrapper);
        courseService.getLiveCourseInfoVos(coursePage,title,startTime,endTime,order,type,teacher.getId());
        List<CourseInfoVo> records = coursePage.getRecords();
        long total = coursePage.getTotal();
        return LinResult.success().data("list",records).data("total",total);
    }

    /**
     * @param courseId
     * @return LinResult
     * @date 2023/3/14 17:49
     * @description 根据courseId删除直播课程信息
     */
    @DeleteMapping("live/{courseId}")
    public LinResult deleteLiveCourse(@PathVariable("courseId") String courseId){
        if (courseService.removeLiveCourse(courseId)) {
            return LinResult.success();
        }
        return LinResult.error();
    }

    /**
     * @param video
     * @return LinResult
     * @date 2023/3/14 11:04
     * @description 更新直播课开始时间
     */
    @PutMapping("startTime")
    public LinResult updateStartTime(@RequestBody Video video){
        if (courseService.updateLiveVideoStartTime(video)) {
            return LinResult.success();
        }
        return LinResult.error();
    }
}

package com.lin.controller.teacher;


import com.lin.entity.vo.ModifyPasswordVo;
import com.lin.service.AclUserService;
import com.lin.utils.JWTUtil;
import com.lin.utils.LinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;


/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/21 13:55
 * @Description 教师个人信息
 */
@RestController
@RequestMapping("user")
@CrossOrigin
public class PersonalInfoController {
    @Autowired
    private AclUserService aclUserService;

    @PostMapping("teacher")
    public LinResult updatePassword(@RequestBody ModifyPasswordVo passwordVo, HttpServletRequest request){
        String teacherId = JWTUtil.getMemberIdByJwtToken(request);
        if (aclUserService.updatePasswordByOldPassword(teacherId,passwordVo.getOldPassword(), passwordVo.getNewPassword()))
            return LinResult.success();
        return LinResult.error();
    }
}

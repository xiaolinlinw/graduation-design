package com.lin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.aop.WebLog;
import com.lin.entity.AclUser;
import com.lin.entity.Course;
import com.lin.entity.Teacher;
import com.lin.entity.Video;
import com.lin.entity.vo.course.CourseInfoVo;
import com.lin.service.AclUserService;
import com.lin.service.CourseService;
import com.lin.service.TeacherService;
import com.lin.utils.JWTUtil;
import com.lin.utils.LinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-23
 */
@RestController
@RequestMapping("/course")
@CrossOrigin
public class CourseController {

    @Autowired
    private CourseService courseService;
    @Autowired
    private AclUserService userService;
    @Autowired
    private TeacherService teacherService;
    /**
     * @param file
     * @return LinResult
     * @date 2023/1/5 11:44
     * @description 上传课程封面接口
     */
    @PostMapping("/uploadCover")
    public LinResult uploadCover(MultipartFile file) throws IOException {
        if (file == null)
            return LinResult.error().message("文件上传失败");
        String url = courseService.saveCover(file);
        return LinResult.success().data("url", url);
    }
    /**
     * @param course
     * @return LinResult
     * @date 2023/1/5 11:49
     * @description 教师上传课程信息
     */
    @PostMapping("/addCourse")
    @WebLog("添加课程")
    public LinResult addCourse(@RequestBody Course course, HttpServletRequest request) {
        String teacherId = JWTUtil.getMemberIdByJwtToken(request);
        AclUser teacherACl = userService.getById(teacherId);
        QueryWrapper<Teacher> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.eq("teacher_no",teacherACl.getUsername());
        teacherQueryWrapper.select("id");
        Teacher teacher  = teacherService.getOne(teacherQueryWrapper);
        course.setTeacherId(teacher.getId());
        if (courseService.save(course)) {
            return LinResult.success().data("courseId", course.getId());
        }
        return LinResult.error();
    }
    /**
     * @param course
     * @return LinResult
     * @date 2023/1/5 11:50
     * @description 修改课程信息
     */
    @PutMapping("/updateCourse")
    public LinResult updateCourse(@RequestBody Course course) {
        if (courseService.updateById(course)) {
            return LinResult.success();
        }
        return LinResult.error();
    }
    /**
     * @param courseId
     * @return LinResult
     * @date 2023/1/5 11:50
     * @description 根据courseId获取课程信息
     */
    @GetMapping("courseInfo/{courseId}")
    public LinResult getCourseInfo(@PathVariable String courseId) {
        CourseInfoVo courseInfoVo = courseService.getCourseInfo(courseId);
        return LinResult.success().data("courseInfo",courseInfoVo);
    }
    /**
     * @param currentPage
     * @param pageSize
     * @param type
     * @param title
     * @param startTime
     * @param endTime
     * @param order
     * @return LinResult
     * @date 2023/1/5 10:26
     * @description 教师查询自己上传课程信息
     */
    @GetMapping("courseInfos")
    @WebLog("获取课程信息")
    public LinResult getCourseInfos(@RequestParam(value = "currentPage",required = false,defaultValue = "1") long currentPage,
                                    @RequestParam(value = "pageSize",required = false,defaultValue = "10") long pageSize,
                                    @RequestParam(value = "type",required = false,defaultValue = "0") Byte type,
                                    @RequestParam(value = "title",required = false) String title,
                                    @RequestParam(value = "startTime",required = false) String startTime,
                                    @RequestParam(value = "endTime",required = false) String endTime,
                                    @RequestParam(value = "order",required = false) String order,
                                    HttpServletRequest request){

        Page<CourseInfoVo> coursePage = new Page<>(currentPage, pageSize);
        String teacherId = JWTUtil.getMemberIdByJwtToken(request);

        AclUser teacherACl = userService.getById(teacherId);
        QueryWrapper<Teacher> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.eq("teacher_no",teacherACl.getUsername());
        teacherQueryWrapper.select("id");
        Teacher teacher  = teacherService.getOne(teacherQueryWrapper);
        courseService.getCourseInfoVos(coursePage,title,startTime,endTime,order,type,teacher.getId());
        List<CourseInfoVo> records = coursePage.getRecords();
        long total = coursePage.getTotal();
        return LinResult.success().data("list",records).data("total",total);
    }
    /**
     * @param courseId
     * @return LinResult
     * @date 2023/1/5 11:51
     * @description 根据courseId删除课程信息
     */
    @DeleteMapping("/{courseId}")
    public LinResult deleteCourse(@PathVariable("courseId") String courseId){
        if (courseService.removeCourse(courseId))
            return LinResult.success();
        return LinResult.error();
    }

    /**
     * @param courseId
     * @return LinResult
     * @date 2023/1/5 11:51
     * @description 根据courseId获取课程基本信息
     */
    @GetMapping("baseInfo/{courseId}")
    public LinResult getCourseEditInfo(@PathVariable("courseId") String courseId){
        Course course = courseService.getById(courseId);
        return LinResult.success().data("courseEditInfo",course);
    }

}

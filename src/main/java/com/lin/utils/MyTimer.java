package com.lin.utils;

import javax.servlet.http.HttpSession;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/24 8:22
 * @Description 定时工具
 */
public class MyTimer {
    public static void timerTask(HttpSession session,String attributeName,int time){
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                session.removeAttribute(attributeName);
                timer.cancel();
            }
        },time*1000L);
    }
}

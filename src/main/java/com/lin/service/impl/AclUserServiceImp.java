package com.lin.service.impl;

import com.aliyun.vod.upload.common.MD5Util;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lin.entity.AclPermission;
import com.lin.entity.AclRole;
import com.lin.entity.AclUser;
import com.lin.entity.Teacher;
import com.lin.exception.LinException;
import com.lin.mapper.AclUserMapper;
import com.lin.service.AclUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.utils.ResultCode;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-21
 */
@Service
public class AclUserServiceImp extends ServiceImpl<AclUserMapper, AclUser> implements AclUserService {

    @Override
    public boolean updatePasswordByOldPassword(String id,String oldPassword, String newPassword) {
        QueryWrapper<AclUser> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.eq("id",id);
        teacherQueryWrapper.eq("password", MD5Util.md5(oldPassword));
        Long count = baseMapper.selectCount(teacherQueryWrapper);
        if (count==0){
            throw new LinException(ResultCode.ERROR,"旧密码错误");
        }
        AclUser aclUser = new AclUser();
        aclUser.setId(id);
        aclUser.setPassword(MD5Util.md5(newPassword));
        return baseMapper.updateById(aclUser)>0;
    }

    @Override
    public List<AclRole> getRoles(String adminId) {

        return baseMapper.selectRoles(adminId);
    }

    @Override
    public List<AclPermission> getPermissions(List<String> roleIds) {
        return baseMapper.selectPermissions(roleIds);
    }
}

package com.lin.controller.user;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.aop.WebLog;
import com.lin.entity.Course;
import com.lin.entity.LiveCourse;
import com.lin.entity.Teacher;
import com.lin.entity.UcenterMember;
import com.lin.entity.vo.chapter.ChapterVo;
import com.lin.entity.vo.front.CourseConditionVo;
import com.lin.entity.vo.front.CourseFrontInfoVo;
import com.lin.service.*;
import com.lin.utils.JWTUtil;
import com.lin.utils.LinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/21 17:35
 * @Description 首页数据
 */
@CrossOrigin
@RestController
@RequestMapping("preschool")
public class PreschoolController {
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private ChapterService chapterService;
    @Autowired
    private LiveCourseService liveCourseService;
    @Autowired
    private UcenterMemberService userService;

    @GetMapping("index")
    public LinResult index(HttpServletRequest request) {
        QueryWrapper<Course> courseQueryWrapper = new QueryWrapper<>();
        String userId = JWTUtil.getMemberIdByJwtToken(request);
        if (!userId.isEmpty()) {
            UcenterMember user = userService.getById(userId);
            courseQueryWrapper.le("age",user.getAge());
            courseQueryWrapper.orderByDesc("age");
        }else {
            courseQueryWrapper.orderByDesc("id");
        }
        courseQueryWrapper.eq("type",0);
        courseQueryWrapper.last("limit 8");
        List<Course> courseList = courseService.list(courseQueryWrapper);


        QueryWrapper<Teacher> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.orderByDesc("id");
        teacherQueryWrapper.last("limit 4");
        List<Teacher> teacherList = teacherService.list(teacherQueryWrapper);
        return LinResult.success().data("courseList", courseList).data("teacherList", teacherList);
    }

    @GetMapping("course/{courseId}")
    public LinResult getCourse(@PathVariable("courseId") String courseId) {
        CourseFrontInfoVo courseFrontInfoVo = courseService.getFrontCourseInfo(courseId);
        List<ChapterVo> chapterVoList = chapterService.getChapter(courseId);
        return LinResult.success().data("chapterVideoList", chapterVoList).data("courseFrontInfo", courseFrontInfoVo).data("courseStatus", true);
    }
    @WebLog("教师")
    @GetMapping("teacher/{teacherId}")
    public LinResult getTeacher(@PathVariable("teacherId") String teacherId) {
        Teacher teacher = teacherService.getById(teacherId);
        QueryWrapper<Course> courseQueryWrapper = new QueryWrapper<>();
        courseQueryWrapper.eq("teacher_id", teacherId);
        List<Course> courseList = courseService.list(courseQueryWrapper);
        return LinResult.success().data("teacher", teacher).data("courseList", courseList);
    }

    @GetMapping("teachers/{currentPage}/{pageSize}")
    public LinResult getTeachers(@PathVariable("currentPage") long currentPage, @PathVariable("pageSize") long pageSize) {
        Page<Teacher> teacherPage = new Page<>(currentPage, pageSize);
        JSONObject map = teacherService.getTeachersPage(teacherPage);
        return LinResult.success().data(map);
    }
    @WebLog("条件查询")
    @PostMapping("courses/{currentPage}/{pageSize}")
    public LinResult getCourse(@RequestBody CourseConditionVo courseConditionVo,
                               @PathVariable("currentPage")
                               long currentPage, @PathVariable("pageSize") long pageSize,
                               @RequestParam(value = "type", required = false, defaultValue = "0") Byte type
    ) {
        Page<Course> coursePage = new Page<>(currentPage, pageSize);
        JSONObject jsonObject = courseService.getCourseInfoByCondition(coursePage, courseConditionVo, type);
        return LinResult.success().data(jsonObject);
    }

    @GetMapping("live")
    public LinResult createLiveCourse(@RequestParam("roomId") String roomId,@RequestParam("videoId") String videoId){
        if (liveCourseService.createLiveURL(roomId,videoId)) {
            return LinResult.success();
        }
        return LinResult.error();
    }
    @GetMapping("livePush/{roomId}")
    public LinResult getLivePushUrl(@PathVariable("roomId") String roomId) {
        QueryWrapper<LiveCourse> liveCourseQueryWrapper = new QueryWrapper<>();
        liveCourseQueryWrapper.eq("room_id",roomId);
        LiveCourse liveCourse = liveCourseService.getOne(liveCourseQueryWrapper);
        return LinResult.success().data("pushUrl",liveCourse.getPushUrl());
    }

    @GetMapping("livePull/{videoId}")
    public LinResult getLivePullUrl(@PathVariable("videoId") String videoId){
        LiveCourse liveCourse = liveCourseService.getById(videoId);
        if (liveCourse==null)
            return LinResult.error().message("未开播");
        return LinResult.success().data("pullUrl",liveCourse.getPullUrl());
    }
}

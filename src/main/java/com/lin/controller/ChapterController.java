package com.lin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lin.entity.Chapter;
import com.lin.entity.vo.chapter.ChapterVo;
import com.lin.service.ChapterService;
import com.lin.utils.LinResult;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-24
 */
@RestController
@RequestMapping("/chapter")
@CrossOrigin
public class ChapterController {
    @Autowired
    private ChapterService chapterService;
    /**
     * @param chapter
     * @return LinResult
     * @date 2023/1/5 11:56
     * @description 添加课程章节信息
     */
    @PostMapping("/addChapter")
    public LinResult addChapter(@RequestBody Chapter chapter){
        if (chapterService.saveChapter(chapter)) {
            return LinResult.success();
        }
        return LinResult.error();
    }
    /**
     * @param courseId
     * @return LinResult
     * @date 2023/1/5 10:24
     * @description 获取该课程课时信息
     */
    @GetMapping("/getChapter/{courseId}")
    public LinResult getChapter(@PathVariable("courseId") String courseId){
        List<ChapterVo> list = chapterService.getChapter(courseId);
        return LinResult.success().data("chapter",list);
    }
    /**
     * @param chapterId
     * @return LinResult
     * @date 2023/1/5 11:57
     * @description 根据id获取章节信息
     */
    @GetMapping("/getChapterById/{chapterId}")
    public LinResult getChapterById(@PathVariable("chapterId") String chapterId){
        Chapter chapter = chapterService.getById(chapterId);
        return LinResult.success().data("chapter",chapter);
    }
    /**
     * @param chapterId
     * @return LinResult
     * @date 2023/1/5 12:02
     * @description 根据id删除章节信息
     */
    @DeleteMapping("/deleteChapter/{chapterId}")
    public LinResult deleteChapter(@PathVariable("chapterId") String chapterId){
        if (chapterService.removeChapter(chapterId)) {
            return LinResult.success();
        }
        return LinResult.error();
    }
    @DeleteMapping("liveChapter/{chapterId}")
    public LinResult deleteLiveChapter(@PathVariable("chapterId")String chapter){
        if (chapterService.removeLiveChapter(chapter)){
            return LinResult.success();
        }
        return LinResult.error();
    }
    /**
     * @param chapter
     * @return LinResult
     * @date 2023/1/5 12:02
     * @description 修改章节信息
     */
    @PutMapping("/updateChapter")
    public LinResult updateChapter(@RequestBody Chapter chapter){
        if (chapterService.updateByChapter(chapter)) {
            return LinResult.success();
        }
        return LinResult.error();
    }
    @PutMapping("swapChapter")
    public LinResult swapUpdateChapter(@RequestBody Chapter chapter){
        if (chapterService.swapUpdateChapter(chapter)){
            return LinResult.success();
        }
        return LinResult.error();
    }
}

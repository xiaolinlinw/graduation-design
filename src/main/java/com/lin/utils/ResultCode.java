package com.lin.utils;

/**
 * Copyright(C),2022年-11月-30,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/11/30 19:22
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/11/30      版本号
 */
public class ResultCode {
    //默认成功
    public static final int SUCCESS=20000;
    // 默认失败
    public static final int ERROR=20001;
    //数据校验失败
    public static final int VALID_FAIL=30001;
    //排序存在冲突
    public static final int SORT_EXISTS=50001;
    //直播课开课时间顺序错误
    public static final int START_TIME_ERROR=60001;

    public static final int UnauthorizedException=403;
    public static final int UnauthenticatedException=401;

    public static final int USER_IS_DISABLE=444;
}

package com.lin.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/30 17:46
 * @Description 展示月份播放量
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class StatisticMonthVo {
    private String date;
    private Long viewVideoNum;
}

package com.lin.service.impl;

import com.lin.entity.AclPermission;
import com.lin.mapper.AclPermissionMapper;
import com.lin.service.AclPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-28
 */
@Service
public class AclPermissionServiceImp extends ServiceImpl<AclPermissionMapper, AclPermission> implements AclPermissionService {

}

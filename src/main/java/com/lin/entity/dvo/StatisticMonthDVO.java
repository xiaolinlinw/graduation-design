package com.lin.entity.dvo;

import lombok.Data;
import lombok.ToString;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/30 17:44
 * @Description 最近12月
 */
@Data
@ToString
public class StatisticMonthDVO {
    private String statisticYear;
    private String statisticMonth;
    private Long videoVideoNum;
}

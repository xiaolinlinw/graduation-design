package com.lin.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.aop.WebLog;
import com.lin.entity.AclRole;
import com.lin.service.AclRoleService;
import com.lin.utils.LinResult;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/30 11:18
 * @Description 角色管理
 */
@RestController
@RequestMapping("admin")
@RequiresRoles("admin")
public class RoleController {
    @Autowired
    private AclRoleService aclRoleService;
    @GetMapping("roles")
    @WebLog("角色管理")
    public LinResult getRoles(@RequestParam(value = "currentPage",defaultValue = "1") long currentPage,
                              @RequestParam(value = "PageSize",defaultValue = "10") long pageSize){
        Page<AclRole> aclRolePage = new Page<>(currentPage,pageSize);

        aclRoleService.page(aclRolePage);
        return LinResult.success().data("list",aclRolePage.getRecords()).data("total",aclRolePage.getTotal());
    }
    @PutMapping("role")
    public LinResult getRoles(@RequestBody AclRole aclRole){
        if (aclRoleService.updateById(aclRole)) {
            return LinResult.success();
        }
        return LinResult.error();
    }
}

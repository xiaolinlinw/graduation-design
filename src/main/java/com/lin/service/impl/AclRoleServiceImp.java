package com.lin.service.impl;

import com.lin.entity.AclRole;
import com.lin.mapper.AclRoleMapper;
import com.lin.service.AclRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-28
 */
@Service
public class AclRoleServiceImp extends ServiceImpl<AclRoleMapper, AclRole> implements AclRoleService {

}

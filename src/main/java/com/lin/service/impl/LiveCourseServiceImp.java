package com.lin.service.impl;

import com.lin.entity.LiveCourse;
import com.lin.mapper.LiveCourseMapper;
import com.lin.service.LiveCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.utils.CreateLiveUrl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 直播课直播地址信息 服务实现类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-22
 */
@Service
public class LiveCourseServiceImp extends ServiceImpl<LiveCourseMapper, LiveCourse> implements LiveCourseService {

    @Override
    public boolean createLiveURL(String roomId, String videoId) {
        String pushKey="abLELhKcMktPIeID";
        String pullKey="E3ttmitS7eFZ8PQ5";
        String appName="xiaokuihua";

        String pushUrl = CreateLiveUrl.generate_push_url(pushKey, appName, roomId, 60 * 1440);
        String pullUrl = CreateLiveUrl.general_pull_url(pullKey, appName, roomId, 60*1440);
        LiveCourse liveCourse = new LiveCourse();

        liveCourse.setId(videoId);
        liveCourse.setRoomId(roomId);
        liveCourse.setPushUrl(pushUrl);
        liveCourse.setPullUrl(pullUrl);

        return baseMapper.insert(liveCourse)>0;
    }
}

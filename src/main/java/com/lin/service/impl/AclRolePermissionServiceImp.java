package com.lin.service.impl;

import com.lin.entity.AclRolePermission;
import com.lin.mapper.AclRolePermissionMapper;
import com.lin.service.AclRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-28
 */
@Service
public class AclRolePermissionServiceImp extends ServiceImpl<AclRolePermissionMapper, AclRolePermission> implements AclRolePermissionService {

}

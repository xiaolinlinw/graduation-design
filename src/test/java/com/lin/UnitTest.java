package com.lin;

import com.aliyun.vod.upload.common.MD5Util;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.entity.Video;
import com.lin.entity.vo.StatisticMonthVo;
import com.lin.entity.vo.StatisticVo;
import com.lin.entity.vo.course.CourseInfoVo;
import com.lin.service.CourseService;
import com.lin.service.StatisticsDailyService;
import com.lin.service.VideoService;
import com.lin.utils.JWTUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.DigestUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.lin.utils.JWTUtil.APP_SECRET;

/**
 * Copyright(C),2022年-12月-28,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/28 13:00
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/28      版本号
 */
@SpringBootTest
public class UnitTest {
    @Autowired
    private CourseService courseService;
    @Autowired
    private VideoService videoService;
    /**
     * @param
     * @return void
     * @date 2023/1/5 9:20
     * @description 自定义SQL分页功能测试
     */
    @Test
    void customSqlPage(){
        Page<CourseInfoVo> courseInfoVoPage = new Page<>(1, 5);
//        courseService.getCourseInfoVos(courseInfoVoPage);
//        System.out.println(courseInfoVoPage.getRecords());
        courseService.getCourseInfoVos(courseInfoVoPage,null,null,null,null, (byte) 0,"1");
    }
    @Test
    void LiveTimeTest(){
        Video video = new Video();
        video.setCourseId("f3e80d6e8b492e73385e2245e8921723");
        video.setChapterId("d278cacf562c4215ec6d2bfab9491554");
        video.setIsFree((byte)1);
        video.setSort(1);
        video.setTitle("live开始时间测试");
        video.setStartTime(LocalDateTime.now());
        courseService.saveLiveVideo(video);
    }
    @Test
    void testDate(){
        Video video = new Video();
        video.setStartTime(LocalDateTime.now());
        System.out.println(video.getStartTime());
    }
    @Test
    void testCount(){
        QueryWrapper<Video> videoQueryWrapper = new QueryWrapper<>();
        videoQueryWrapper.select("course_id");
        videoQueryWrapper.eq("course_id","23");
        System.out.println(videoService.count(videoQueryWrapper));
    }
    @Test
    void testMD5(){
        String str="1234";
        System.out.println(MD5Util.md5(str));
        System.out.println(DigestUtils.md5DigestAsHex(str.getBytes()));
    }

    @Test
    void testToken(){
        String token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJsaW4tdXNlciIsImlhdCI6MTY3OTY0MDAwMiwiZXhwIjoxNjc5NzI2NDAyLCJpZCI6IjRkOTE1MmY0Y2I1ZDk3Y2E4MzE4NmZlYzZiNWRhYmUwIn0.RJwz_m3XOCD-0l9pjZBuLm31n8Av1-et9HtMuextAV4";
        Jws<Claims> claimsJws =
                Jwts.parser().setSigningKey(APP_SECRET).parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        System.out.println(claims.get("id"));
    }
    @Autowired
    private StatisticsDailyService statisticsDailyService;

    @Test
    void streamTest(){
        List<StatisticVo> statisticInfo = statisticsDailyService.getStatisticInfo();
        statisticInfo.forEach(System.out::println);
    }

    @Test
    void streamTest2(){
        List<StatisticMonthVo> statisticMonthInfo = statisticsDailyService.getStatisticMonthInfo();
        System.out.println("size"+statisticMonthInfo.size());
        statisticMonthInfo.forEach(x-> System.out.println("===>"+x));
    }

    @Test
    void testDate2(){
        LocalDateTime localDateTime = LocalDateTime.now().plusDays(-1);
        System.out.println(localDateTime);
        System.out.println(LocalDate.now());
    }
}

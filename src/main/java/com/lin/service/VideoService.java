package com.lin.service;

import com.aliyuncs.exceptions.ClientException;
import com.lin.entity.Video;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-24
 */
public interface VideoService extends IService<Video> {

    String saveVideo(MultipartFile file) throws IOException;

    boolean deleteVideo(String videoId);
    boolean deleteVideoBatch(String chapterId);
    boolean removeVideoBatch(String courseId);

    /**
     * @param video
     * @param conflictVideo
     * @return boolean
     * @date 2023/1/6 12:07
     * @description 更新时交换课时顺序
     */
    boolean swapUpdateVideo(Video video, Video conflictVideo);

    LocalDateTime getPreVideo(Video video);

    LocalDateTime getNextVideo(Video video);

    String getPlayAuth(String videoSourceId) throws ClientException;

    boolean updatePlayCount(String videoSourceId);

    Long statisticsCount(LocalDateTime yesterdays);
}

package com.lin.entity;

import com.baomidou.mybatisplus.annotation.*;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 课程科目
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-09
 */
public class Subject implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程类别ID
     */
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 类别名称
     */
    private String title;

    /**
     * 父I分类ID，顶级类别为0
     */
    private String parentId;

    /**
     * 排序字段
     */
    private Integer sort;

    /**
     * 0，未删除，1、删除
     */
//    @TableLogic
    private Integer isDelete;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public LocalDateTime getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(LocalDateTime gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public LocalDateTime getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(LocalDateTime gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "Subject{" +
            "id = " + id +
            ", title = " + title +
            ", parentId = " + parentId +
            ", sort = " + sort +
            ", isDelete = " + isDelete +
            ", gmtCreate = " + gmtCreate +
            ", gmtModified = " + gmtModified +
        "}";
    }
}

package com.lin.controller;

import com.lin.entity.StatisticsDaily;
import com.lin.entity.vo.StatisticVo;
import com.lin.service.StatisticsDailyService;
import com.lin.utils.LinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-30
 */
@RestController
@RequestMapping("statistics")
public class StatisticsDailyController {
    @Autowired
    private StatisticsDailyService statisticsDailyService;
    @GetMapping()
    public LinResult getStatistic(){
        List<StatisticVo> statisticInfos =  statisticsDailyService.getStatisticInfo();
        return LinResult.success().data("statisticInfos",statisticInfos);
    }

    @GetMapping("/trend")
    public LinResult getTrend(){
        return LinResult.success().data("statistic",statisticsDailyService.getStatisticTrendInfo());
    }

    @GetMapping("month")
    public LinResult getMonthStatistic(){
        return LinResult.success().data("statistic",statisticsDailyService.getStatisticMonthInfo());
    }

}

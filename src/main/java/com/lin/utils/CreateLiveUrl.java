package com.lin.utils;

import org.apache.commons.lang.RandomStringUtils;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/22 20:55
 * @Description 生成推流及播流地址
 */
public class CreateLiveUrl {

    /**
     * 计算md5
     *
     * @param param
     * @return
     */
    public static String md5(String param) {
        if (param == null || param.length() == 0) {
            return null;
        }
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(param.getBytes());
            byte[] byteArray = md5.digest();

            BigInteger bigInt = new BigInteger(1, byteArray);
            // 参数16表示16进制
            String result = bigInt.toString(16);
            // 不足32位高位补零
            while (result.length() < 32) {
                result = "0" + result;
            }
            return result;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 生成推流地址
     *
     * 推流域名
     * @param pushKey    推流域名配置的鉴权Key
     * @param appName    推流AppName
     * @param streamName 推流StreamName
     * @param expireTime 过期时间（单位是秒）
     */
    public static String generate_push_url(String pushKey, String appName, String streamName, long expireTime) {
        String pushDomain="livepush.linlin.online";
        String pushUrl = "";
        //推流域名未开启鉴权功能的情况下
        if (Objects.equals(pushKey, "")) {
            pushUrl = "artc://" + pushDomain + "/" + appName + "/" + streamName;
        } else {
            long timeStamp = System.currentTimeMillis() / 1000L + expireTime;
            String stringToMd5 = "/" + appName + "/" + streamName + "-" + timeStamp + "-0-0-" + pushKey;
            String authKey = md5(stringToMd5);
            pushUrl = "artc://" + pushDomain + "/" + appName + "/" + streamName + "?auth_key=" + timeStamp + "-0-0-" + authKey;
        }
        System.out.println("推流地址是： " + pushUrl);
        return pushUrl;
    }

    /**
     * 生成播放地址
     *
     * pullDomain 播放域名
     * @param pullKey    播放鉴权Key
     * @param appName    播放appName（同推流appName)
     * @param streamName 播放streamName 同推流streamName）
     * @param expireTime 过期时间（单位是秒
     */
    public static String general_pull_url(String pullKey, String appName, String streamName, long expireTime) {
        String pullDomain="livepull.linlin.online";
        String rtmpUrl = ""; //rtmp的拉流地址
        String rtsUrl = ""; //rts的拉流地址
        //播放域名未配置鉴权Key的情况下
        if (Objects.equals(pullKey, "")) {
            rtmpUrl = "rtmp://" + pullDomain + "/" + appName + "/" + streamName;
            rtsUrl = "artc://" + pullDomain + "/" + appName + "/" + streamName;
        } else {
            long timeStamp = System.currentTimeMillis() / 1000L + expireTime;

            String rtmpToMd5 = "/" + appName + "/" + streamName + "-" + timeStamp + "-0-0-" + pullKey;
            String rtmpAuthKey = md5(rtmpToMd5);
            rtmpUrl = "rtmp://" + pullDomain + "/" + appName + "/" + streamName + "?auth_key=" + timeStamp + "-0-0-" + rtmpAuthKey;

            String rtsToMd5 = "/" + appName + "/" + streamName + "-" + timeStamp + "-0-0-" + pullKey;
            String rtsAuthKey = md5(rtsToMd5);
            rtsUrl = "artc://" + pullDomain + "/" + appName + "/" + streamName + "?auth_key=" + timeStamp + "-0-0-" + rtsAuthKey;
        }
        System.out.println("RTMP播放地址为： " + rtmpUrl);
        System.out.println("rts播放地址为： " + rtsUrl);
        return rtsUrl;
    }


    public static void main(String[] args) {
        //生成长度为5的随机字符串作为appName和streamName（字母和数字组合）
        String appName = RandomStringUtils.randomAlphanumeric(5);
        String streamName = RandomStringUtils.randomAlphanumeric(5);


        long expireTime = 60*1440;
        String pushKey = "abLELhKcMktPIeID";

        String pullKey = "E3ttmitS7eFZ8PQ5";
        CreateLiveUrl.general_pull_url( pullKey, appName, streamName, expireTime);
        CreateLiveUrl.generate_push_url(pushKey, appName, streamName, expireTime);
    }
}

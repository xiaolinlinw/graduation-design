package com.lin.controller;

import com.lin.entity.Video;
import com.lin.service.CourseService;
import com.lin.service.VideoService;
import com.lin.utils.LinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-24
 */
@RestController
@RequestMapping("/video")
@CrossOrigin
public class VideoController {
    @Autowired
    private VideoService videoService;
    @Autowired
    private CourseService courseService;

    /**
     * @param file
     * @return LinResult
     * @date 2023/1/5 12:04
     * @description 课程视频上传接口
     */
    @PostMapping("/uploadVideo")
    public LinResult uploadVideo(MultipartFile file) throws IOException {
        if (file != null) {
            String videoSourceId = videoService.saveVideo(file);
            return LinResult.success().data("videoSourceId", videoSourceId);
        }
        return LinResult.error();
    }

    /**
     * @param video
     * @return LinResult
     * @date 2023/1/5 12:04
     * @description 添加课时信息
     */
    @PostMapping("addVideo")
    public LinResult addVideo(@RequestBody Video video) {
        if (courseService.saveLiveVideo(video)) {
            return LinResult.success();
        }
        return LinResult.error();
    }

    /**
     * @param video
     * @return LinResult
     * @date 2023/1/5 12:04
     * @description 添加直播课时信息
     */
    @PostMapping("liveVideo")
    public LinResult addLiveVideo(@RequestBody Video video) {
        if (courseService.saveLiveVideo(video)) {
            return LinResult.success();
        }
        return LinResult.error();
    }

    /**
     * @param video
     * @return LinResult
     * @date 2023/1/5 12:05
     * @description 修改课时信息
     */
    @PutMapping("updateVideo")
    public LinResult updateVideo(@RequestBody Video video) {
        if (courseService.updateLiveVideo(video))
            return LinResult.success();
        return LinResult.error();
    }

    @PutMapping("swapVideo")
    public LinResult swapUpdateVideo(@RequestBody Video video) {
        if (courseService.swapUpdateLiveVideo(video)){
            return LinResult.success();
        }
        return LinResult.error();
    }

    /**
     * @param videoId
     * @return LinResult
     * @date 2023/1/5 12:05
     * @description 根据id获取课时信息
     */
    @GetMapping("video/{videoId}")
    public LinResult getVideo(@PathVariable("videoId") String videoId) {
        Video video = videoService.getById(videoId);
        return LinResult.success().data("video", video);
    }

    /**
     * @param videoId
     * @return LinResult
     * @date 2023/1/5 12:05
     * @description 根据id删除课时信息
     */
    @DeleteMapping("video/{videoId}")
    public LinResult deleteVideo(@PathVariable("videoId") String videoId) {
        if (videoService.deleteVideo(videoId)) {
            return LinResult.success();
        }
        return LinResult.error();
    }
    @DeleteMapping("liveVideo/{videoId}")
    public LinResult deleteLiveVideo(@PathVariable("videoId") String videoId){
        if (videoService.removeById(videoId))
            return LinResult.success();
        return LinResult.error();
    }
}

package com.lin.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.entity.UcenterMember;
import com.lin.service.UcenterMemberService;
import com.lin.utils.LinResult;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/29 16:47
 * @Description 管理用户
 */
@RestController
@CrossOrigin
@RequestMapping("admin")
public class UserController {
    @Autowired
    private UcenterMemberService userService;

    @RequiresRoles("admin")
    @GetMapping("/users")
    public LinResult getUsers(@RequestParam(value = "currentPage", defaultValue = "1") long currentPage,
                              @RequestParam(value = "pageSize",defaultValue = "10") long pageSize,
                              @RequestParam(value = "nickname",required = false) String nickname,
                              @RequestParam(value = "age",required = false) Byte age,
                              @RequestParam(value = "startTime",required = false) String startTime,
                              @RequestParam(value = "endTime", required = false) String endTime,
                              @RequestParam(value = "order", required = false) String order) {

        LambdaQueryWrapper<UcenterMember> ucenterMemberLambdaQueryWrapper = new LambdaQueryWrapper<>();
        ucenterMemberLambdaQueryWrapper.like(StringUtils.isNotBlank(nickname),UcenterMember::getNickname,nickname);
        ucenterMemberLambdaQueryWrapper.eq(age!=null&&age>0,UcenterMember::getAge,age);
        ucenterMemberLambdaQueryWrapper.between(StringUtils.isNotBlank(startTime),UcenterMember::getGmtCreate,startTime,endTime);
        ucenterMemberLambdaQueryWrapper.orderByDesc(StringUtils.isNotBlank(order),UcenterMember::getGmtCreate);

        Page<UcenterMember> usersPage = new Page<>(currentPage, pageSize);
        userService.page(usersPage);
        return LinResult.success().data("list",usersPage.getRecords()).data("total",usersPage.getTotal());
    }

    @PutMapping("user")
    public LinResult disableUser(@RequestBody UcenterMember user){
        if (userService.updateById(user)) {
            return LinResult.success().message("支付成功");
        }
        return LinResult.error();
    }
}

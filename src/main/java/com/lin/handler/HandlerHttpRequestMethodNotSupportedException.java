package com.lin.handler;

import com.lin.utils.LinResult;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Copyright(C),2022年-12月-05,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/5 10:45
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/5      版本号
 */
@RestControllerAdvice
public class HandlerHttpRequestMethodNotSupportedException {
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public LinResult handlerHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e){
        return LinResult.error().message(e.getMessage());
    }
}

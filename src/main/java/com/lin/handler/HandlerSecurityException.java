package com.lin.handler;

import com.lin.utils.LinResult;
import com.lin.utils.ResultCode;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/29 8:51
 * @Description 认证授权失败处理
 */
@RestControllerAdvice
public class HandlerSecurityException {
    @ExceptionHandler(UnauthenticatedException.class)
    public LinResult UnauthenticatedExceptionHandler(Exception e){
        return LinResult.error().code(ResultCode.UnauthenticatedException).message("登录认证失败");
    }
    @ExceptionHandler(UnauthorizedException.class)
    public LinResult UnauthorizedExceptionHandler(Exception e){
        return LinResult.error().code(ResultCode.UnauthorizedException).message("无权限"+e.getMessage());
    }
    @ExceptionHandler(AuthenticationException.class)
    public LinResult AuthenticationExceptionHandler(Exception e){
        return LinResult.error().code(ResultCode.UnauthenticatedException).message(e.getMessage());
    }
}

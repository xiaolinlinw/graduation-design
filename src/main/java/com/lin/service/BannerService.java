package com.lin.service;

import com.lin.entity.Banner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-21
 */
public interface BannerService extends IService<Banner> {

}

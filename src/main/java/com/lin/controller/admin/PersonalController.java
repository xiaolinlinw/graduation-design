package com.lin.controller.admin;

import com.lin.entity.AclUser;
import com.lin.entity.vo.ModifyPasswordVo;
import com.lin.service.AclUserService;
import com.lin.utils.JWTUtil;
import com.lin.utils.LinResult;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/29 20:52
 * @Description 管理员个人设置
 */
@RestController
@RequestMapping("admin")
public class PersonalController {
    @Autowired
    private AclUserService aclUserService;
    @RequiresRoles("admin")
    @PutMapping("modify")
    public LinResult modifyPassword(@RequestBody ModifyPasswordVo modifyPasswordVo, HttpServletRequest request){
        String adminId = JWTUtil.getMemberIdByJwtToken(request);
        if (aclUserService.updatePasswordByOldPassword(adminId,modifyPasswordVo.getOldPassword(),modifyPasswordVo.getNewPassword())) {
            return LinResult.success();
        }
        return LinResult.error();
    }
    @RequiresRoles("admin")
    @PutMapping("admin")
    public LinResult updateInfo(@RequestBody AclUser aclUser){
        if (aclUserService.updateById(aclUser)) {
            return LinResult.success();
        }
        return LinResult.error();
    }
}

package com.lin.entity.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * Copyright(C),2022年-12月-09,霖霖
 * FileNames:FileController
 * Author:霖霖
 * Date:2022/12/9 16:37
 * Description:
 * History:
 * <author>       <time>      <version>
 * 霖霖            2022/12/9      版本号
 */
@Data
@ToString
@NoArgsConstructor
public class Classification implements Serializable {
    private String title;
    private String key;
    private List<ClassificationTwo> children;
    private int level=1;
}

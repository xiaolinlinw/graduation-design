package com.lin.mapper;

import com.lin.entity.Video;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDateTime;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author 霖霖
 * @since 2022-12-24
 */
@Mapper
public interface VideoMapper extends BaseMapper<Video> {

    long updatePlayCount(String videoSourceId);

    Long selectStatisticCount(LocalDateTime yesterdays);
}

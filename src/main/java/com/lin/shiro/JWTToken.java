package com.lin.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/29 9:06
 * @Description shiro 在没有和 jwt 整合之前，用户的账号密码被封装成了 UsernamePasswordToken 对象，UsernamePasswordToken 其实是 AuthenticationToken 的实现类。
 */
public class JWTToken implements AuthenticationToken {
    private String loginType;
    public JWTToken(String token,String loginType) {
        this.token = token;
        this.loginType=loginType;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private String token;

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}

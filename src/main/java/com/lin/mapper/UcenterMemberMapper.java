package com.lin.mapper;

import com.lin.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDateTime;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-23
 */
@Mapper
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {

    Long selectStatisticCount(LocalDateTime yesterday);
}

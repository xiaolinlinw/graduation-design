package com.lin.aop;

import java.lang.annotation.*;

/**
 * @author 霖霖
 * @version 1.0
 * @date 2023/3/30 20:38
 * @Description 自定义日志注解
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface WebLog {
    /**
     * 日志描述信息
     * @return
     */
    String value() default "";

}

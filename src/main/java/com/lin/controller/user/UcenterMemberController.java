package com.lin.controller.user;

import com.aliyun.vod.upload.common.MD5Util;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lin.aop.WebLog;
import com.lin.entity.UcenterMember;
import com.lin.entity.vo.ModifyPasswordVo;
import com.lin.service.CourseService;
import com.lin.service.UcenterMemberService;
import com.lin.utils.JWTUtil;
import com.lin.utils.LinResult;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.security.util.Password;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author 霖霖
 * @since 2023-03-23
 */
@RestController
@RequestMapping("/user")
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService userService;
    @WebLog("登录时调用")
    @GetMapping("userInfo")
    public LinResult getUserInfo(HttpServletRequest request) {
        String userId = JWTUtil.getMemberIdByJwtToken(request);
        UcenterMember user = userService.getById(userId);
        if (StringUtils.isBlank(userId))
            return LinResult.error().message("查询异常");
        return LinResult.success().data("nickName", user.getNickname()).data("userId", userId).data("avatar", user.getAvatar());
    }

    @WebLog("获取UserInfo")
    @GetMapping("userAllInfo")
    public LinResult getUserAllInfo(HttpServletRequest request) {
        String userId = JWTUtil.getMemberIdByJwtToken(request);
        UcenterMember user = userService.getById(userId);
        return LinResult.success().data("userInfo", user);
    }

    @PostMapping("password")
    public LinResult modifyPassword(@RequestBody ModifyPasswordVo modifyPasswordVo, HttpServletRequest request) {
        String userId = JWTUtil.getMemberIdByJwtToken(request);
        QueryWrapper<UcenterMember> ucenterMemberQueryWrapper = new QueryWrapper<>();
        ucenterMemberQueryWrapper.eq("id", userId);
        ucenterMemberQueryWrapper.eq("password", MD5Util.md5(modifyPasswordVo.getOldPassword()));
        UcenterMember one = userService.getOne(ucenterMemberQueryWrapper);
        if (one == null) {
            return LinResult.error().message("旧密码不正确");
        }
        one.setPassword(MD5Util.md5(modifyPasswordVo.getNewPassword()));
        if (userService.updateById(one)) {
            return LinResult.success().message("密码更改成功");
        } else {
            return LinResult.error();
        }
    }

    @PutMapping("userInfo")
    public LinResult modifyUserInfo(@RequestBody UcenterMember ucenterMember, HttpServletRequest request) {
        String userId = JWTUtil.getMemberIdByJwtToken(request);
        ucenterMember.setId(userId);
        if (userService.updateById(ucenterMember)) {
            return LinResult.success();
        }
        return LinResult.error();
    }

    @Autowired
    private CourseService courseService;

    @PostMapping("avatar")
    public LinResult uploadAvatar(MultipartFile file, HttpServletRequest request) throws IOException {
        String url = courseService.saveCover(file);
        UcenterMember ucenterMember = new UcenterMember();
        ucenterMember.setId(JWTUtil.getMemberIdByJwtToken(request));
        ucenterMember.setAvatar(url);
        if (userService.updateById(ucenterMember)) {
            return LinResult.success().data("url", url);
        }
        return LinResult.error();
    }
}
